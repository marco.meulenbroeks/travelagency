﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelAgency
{
    public class Booking
    {
        public AirLine AirLine
        {
            get;
        }

        public BookingStatus Status
        {
            get;
        }

        public int NumberOfSeats
        {
            get;
        }

        public decimal PricePerSeat
        {
            get;
        }

        public decimal TotalPrice
        {
            get { return NumberOfSeats * PricePerSeat; }
        }

        public Booking(AirLine airLine, int numberOfSeats, decimal pricePerSeat, BookingStatus status)
        {
            this.AirLine = airLine;
            this.NumberOfSeats = numberOfSeats;
            this.PricePerSeat = pricePerSeat;
            this.Status = status;
        }

        public override string ToString()
        {
            return $"Booking at {this.AirLine.Name} for {NumberOfSeats} of seats costing {PricePerSeat} euro a piece.";
        }
    }
}
