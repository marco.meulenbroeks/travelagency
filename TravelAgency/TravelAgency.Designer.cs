﻿namespace TravelAgency
{
    partial class TravelAgency
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxAirplanes = new System.Windows.Forms.ListBox();
            this.textBoxNumberOfTickets = new System.Windows.Forms.TextBox();
            this.labelAvailableAirplanes = new System.Windows.Forms.Label();
            this.labelNumberOfTickets = new System.Windows.Forms.Label();
            this.buttonBuyTickets = new System.Windows.Forms.Button();
            this.labelUserWelcome = new System.Windows.Forms.Label();
            this.listBoxBookingResult = new System.Windows.Forms.ListBox();
            this.labelBookingResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listBoxAirplanes
            // 
            this.listBoxAirplanes.FormattingEnabled = true;
            this.listBoxAirplanes.Location = new System.Drawing.Point(150, 48);
            this.listBoxAirplanes.Name = "listBoxAirplanes";
            this.listBoxAirplanes.Size = new System.Drawing.Size(641, 95);
            this.listBoxAirplanes.TabIndex = 2;
            // 
            // textBoxNumberOfTickets
            // 
            this.textBoxNumberOfTickets.Location = new System.Drawing.Point(150, 149);
            this.textBoxNumberOfTickets.Name = "textBoxNumberOfTickets";
            this.textBoxNumberOfTickets.Size = new System.Drawing.Size(100, 20);
            this.textBoxNumberOfTickets.TabIndex = 3;
            // 
            // labelAvailableAirplanes
            // 
            this.labelAvailableAirplanes.AutoSize = true;
            this.labelAvailableAirplanes.Location = new System.Drawing.Point(16, 48);
            this.labelAvailableAirplanes.Name = "labelAvailableAirplanes";
            this.labelAvailableAirplanes.Size = new System.Drawing.Size(96, 13);
            this.labelAvailableAirplanes.TabIndex = 4;
            this.labelAvailableAirplanes.Text = "Available Airplanes";
            // 
            // labelNumberOfTickets
            // 
            this.labelNumberOfTickets.AutoSize = true;
            this.labelNumberOfTickets.Location = new System.Drawing.Point(16, 149);
            this.labelNumberOfTickets.Name = "labelNumberOfTickets";
            this.labelNumberOfTickets.Size = new System.Drawing.Size(94, 13);
            this.labelNumberOfTickets.TabIndex = 5;
            this.labelNumberOfTickets.Text = "Number of Tickets";
            // 
            // buttonBuyTickets
            // 
            this.buttonBuyTickets.Location = new System.Drawing.Point(19, 185);
            this.buttonBuyTickets.Name = "buttonBuyTickets";
            this.buttonBuyTickets.Size = new System.Drawing.Size(75, 23);
            this.buttonBuyTickets.TabIndex = 6;
            this.buttonBuyTickets.Text = "Buy Tickets";
            this.buttonBuyTickets.UseVisualStyleBackColor = true;
            this.buttonBuyTickets.Click += new System.EventHandler(this.buttonBuyTickets_Click);
            // 
            // labelUserWelcome
            // 
            this.labelUserWelcome.AutoSize = true;
            this.labelUserWelcome.Location = new System.Drawing.Point(16, 13);
            this.labelUserWelcome.Name = "labelUserWelcome";
            this.labelUserWelcome.Size = new System.Drawing.Size(166, 13);
            this.labelUserWelcome.TabIndex = 7;
            this.labelUserWelcome.Text = "Dynamic text to welcome the user";
            // 
            // listBoxBookingResult
            // 
            this.listBoxBookingResult.FormattingEnabled = true;
            this.listBoxBookingResult.Location = new System.Drawing.Point(150, 226);
            this.listBoxBookingResult.Name = "listBoxBookingResult";
            this.listBoxBookingResult.Size = new System.Drawing.Size(641, 121);
            this.listBoxBookingResult.TabIndex = 8;
            // 
            // labelBookingResult
            // 
            this.labelBookingResult.AutoSize = true;
            this.labelBookingResult.Location = new System.Drawing.Point(19, 226);
            this.labelBookingResult.Name = "labelBookingResult";
            this.labelBookingResult.Size = new System.Drawing.Size(72, 13);
            this.labelBookingResult.TabIndex = 9;
            this.labelBookingResult.Text = "Booked seats";
            // 
            // TravelAgency
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelBookingResult);
            this.Controls.Add(this.listBoxBookingResult);
            this.Controls.Add(this.labelUserWelcome);
            this.Controls.Add(this.buttonBuyTickets);
            this.Controls.Add(this.labelNumberOfTickets);
            this.Controls.Add(this.labelAvailableAirplanes);
            this.Controls.Add(this.textBoxNumberOfTickets);
            this.Controls.Add(this.listBoxAirplanes);
            this.Name = "TravelAgency";
            this.Text = "Travel Agency";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox listBoxAirplanes;
        private System.Windows.Forms.TextBox textBoxNumberOfTickets;
        private System.Windows.Forms.Label labelAvailableAirplanes;
        private System.Windows.Forms.Label labelNumberOfTickets;
        private System.Windows.Forms.Button buttonBuyTickets;
        private System.Windows.Forms.Label labelUserWelcome;
        private System.Windows.Forms.ListBox listBoxBookingResult;
        private System.Windows.Forms.Label labelBookingResult;
    }
}

