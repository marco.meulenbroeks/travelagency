﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelAgency.Data
{
    class UserData
    {
        private static List<RegisteredUser> registeredUsers = new List<RegisteredUser>()
        { 
            new RegisteredUser("Marco", "pass"),
            new RegisteredUser("Jan", "pass")
        };

        public static bool Authenticate(string username, string password)
        {
            bool result = false;

            foreach (RegisteredUser registeredUser in registeredUsers)
            {
                if (registeredUser.Username == username && registeredUser.Password == password)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        class RegisteredUser
        {
            public string Username { get; }
            public string Password { get; }
            public RegisteredUser(string username, string password)
            {
                this.Username = username;
                this.Password = password;
            }
        }
    }
}
