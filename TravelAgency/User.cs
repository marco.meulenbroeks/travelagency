﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelAgency.Data;

namespace TravelAgency
{
    public class User
    {
        private string username;
        private decimal creditCardBalance = 0;

        public string UserName
        {
            get { return username; }
        }

        public decimal CreditCardBalance
        {
            get { return creditCardBalance; }
        }

        public User(string username)
        {
            this.username = username;
        }

        public User(string username, decimal creditCardBalance) : this(username)
        {
            this.creditCardBalance = creditCardBalance;
        }

        public bool Login(string password)
        {
            return UserData.Authenticate(username, password);
        }

        public bool Debit(Booking booking)
        {
            if (this.CreditCardBalance < booking.TotalPrice)
            {
                return false;
            }
            else
            {
                this.creditCardBalance -= booking.TotalPrice;
                return true;
            }
        }
    }
}
