﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelAgency
{
    public class AirLine
    {
        private string name;
        private List<Airplane> airplanes = new List<Airplane>();

        public string  Name
        {
            get { return name; }
        }

        public IReadOnlyList<Airplane> Airplanes
        {
            get { return airplanes; }
        }

        public AirLine(string name)
        {
            this.name = name;
        }

        public void AddAirplane(Airplane airplane)
        {
            this.airplanes.Add(airplane);
        }
    }
}
