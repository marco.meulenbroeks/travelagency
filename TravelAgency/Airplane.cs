﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelAgency
{
    public class Airplane
    {
        private int numberOfSeats = 300;
        private decimal pricePerSeat;
        private AirLine airLine;

        public int NumberOfSeatsAvailable
        {
            get { return numberOfSeats; }
        }

        public decimal PricePerSeat
        {
            get { return pricePerSeat; }
        }

        public AirLine AirLine
        {
            get { return airLine; }
        }

        public Airplane(AirLine airLine, decimal pricePerSeat)
        {
            this.pricePerSeat = pricePerSeat;
            this.airLine = airLine;
        }

        public Airplane(AirLine airLine, decimal pricePerSeat, int numberOfSeats) : this(airLine, pricePerSeat)
        {
            this.numberOfSeats = numberOfSeats;
        }

        public Booking BookSeats(User user, int quantity)
        {
            if (quantity > this.numberOfSeats)
            {
                return new Booking(this.AirLine, quantity, this.PricePerSeat, BookingStatus.NotEnoughSeats);
            }
            if (user.CreditCardBalance < (quantity * this.PricePerSeat))
            {
                return new Booking(this.AirLine, quantity, this.PricePerSeat, BookingStatus.BalanceTooLow);
            }
            this.numberOfSeats -= quantity;
            Booking booking = new Booking(this.AirLine, quantity, this.PricePerSeat, BookingStatus.Accepted);
            user.Debit(booking);
            return booking;
        }

        public override string ToString()
        {
            return $"{airLine.Name} has {numberOfSeats} seats for {pricePerSeat} euro per seat.";
        }
    }
}
