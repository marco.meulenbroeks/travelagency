﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelAgency
{
    public partial class TravelAgency : Form
    {
        private User currentUser;

        private AirLine klm = new AirLine("KLM");
        private AirLine quantas = new AirLine("Quantas");
        private AirLine easyJet = new AirLine("Easy Jet");

        private Airplane airplane1;
        private Airplane airplane2;
        private Airplane airplane3;

        public TravelAgency(User user)
        {
            InitializeComponent();

            currentUser = user;

            airplane1 = new Airplane(klm, 300.99m, 200);
            klm.AddAirplane(airplane1);
            airplane2 = new Airplane(klm, 299.99m, 200);
            klm.AddAirplane(airplane2);
            airplane3 = new Airplane(easyJet, 99.99m);
            easyJet.AddAirplane(airplane3);

            this.UpdateFormData();
        }

        private void UpdateFormData()
        {
            this.labelUserWelcome.Text = $"Welcome {currentUser.UserName}! Your credit card balance is {currentUser.CreditCardBalance} euro.";

            listBoxAirplanes.Items.Clear();

            List<Airplane> airplanesList = new List<Airplane>() {
                airplane1, airplane2, airplane3
            };

            listBoxAirplanes.Items.AddRange(airplanesList.ToArray());
        }

        private void buttonBuyTickets_Click(object sender, EventArgs e)
        {
            Airplane selectedAirplane = (Airplane)listBoxAirplanes.SelectedItem;
            int quantity;
            if (selectedAirplane == null || int.TryParse(textBoxNumberOfTickets.Text, out quantity) == false)
            {
                MessageBox.Show("Please select an airplane and provide the number of tickets you want to buy.");
                return;
            }

            Booking booking = selectedAirplane.BookSeats(this.currentUser, quantity);

            if (booking.Status == BookingStatus.NotEnoughSeats)
            {
                MessageBox.Show($"You tried to book {booking.NumberOfSeats} seats while there are only {selectedAirplane.NumberOfSeatsAvailable} seats available");
                return;
            }

            if (booking.Status == BookingStatus.BalanceTooLow)
            {
                MessageBox.Show($"You tried to book {booking.NumberOfSeats} seats for in total {booking.TotalPrice} while you only have {currentUser.CreditCardBalance} euro in your account");
                return;
            }

            listBoxBookingResult.Items.Add(booking);
            this.UpdateFormData();
        }
    }
}
